import java.util.concurrent.locks.*;
import java.util.concurrent.locks.AbstractQueuedSynchronizer.ConditionObject.*; 




public class BankAccount
{
    private double balance;
    private Lock changeDataLock;
    private Condition negative;

    public BankAccount()
    {
        balance = 0;
	changeDataLock = new ReentrantLock();
	negative = changeDataLock.newCondition();
    }

    public void deposit(double amount)
    {
	changeDataLock.lock();
		
	try{
        System.out.print("Depositing " + amount);
        double newBalance = balance + amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;

	negative.signalAll();
	} finally {
	changeDataLock.unlock();
	}
    }

    public void withdraw(double amount)
    {
	changeDataLock.lock();
	try {
	while(balance < amount) 
	    negative.await();
	
	    System.out.print("Withdrawing " + amount);

	    double newBalance = balance - amount;
	    System.out.println(", new balance is " + newBalance);

	    balance = newBalance;
	
	} catch (InterruptedException Ex) {}
	finally {
       	changeDataLock.unlock();
	}
    }

    public double getBalance()
    {
        return balance;
    }
}
